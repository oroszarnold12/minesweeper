//Nev:Orosz Arnold-Daniel
//Csoport:513
//Azonosito:oaim1856

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Menu extends JPanel {
    private Button nw=new Button("New Game") ;
    private Button e=new Button("Exit");
    private Button co=new Button("Continue");
    private Label l=new Label("Minesweeper");
    private TextField textField=new TextField("Size");
    private String filenev="allapot.txt";
    private int clicked[][];
    private int second[][];
    private int third[][];
    private boolean jatek;
    private int ibszam;
    private int secondsPassed;
    private int size;

    public Menu(CardLayout c,JPanel jp,Panel p){
        co.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    FileInputStream f = new FileInputStream(filenev);
                    ObjectInputStream o = new ObjectInputStream(f);
                    size=(int) o.readObject();
                    clicked=new int [size][size];
                    second=new int [size][size];
                    third=new int [size][size];
                    clicked= (int[][]) o.readObject();
                    second= (int[][]) o.readObject();
                    third= (int[][]) o.readObject();
                    jatek= (boolean) o.readObject();
                    ibszam= (int) o.readObject();
                    secondsPassed=(int) o.readObject();

                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
                p.setSize(size);
                p.setk(clicked);
                p.setm(second);
                p.seth(third);
                p.setj(jatek);
                p.seti(ibszam);
                p.setsp(secondsPassed);
                p.startTimer();
                c.show(jp,"2");
            }
        });
        e.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        nw.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int size=Integer.parseInt(textField.getText());
                if(size<15||size>50){
                    System.out.println("Size should be between 15 and 50");
                }else {
                    p.setSize(size);
                    p.newgame();
                    c.show(jp, "2");
                }
            }
        });

        nw.setBounds(10,10,10,10);
        this.setLayout(new GridLayout(4,1));

        JPanel p1=new JPanel();
        JPanel p2=new JPanel();
        JPanel p3=new JPanel();
        JPanel p4=new JPanel();
        p1.setBackground(new Color(10,70,250));
        p2.setBackground(new Color(10,70,200));
        p3.setBackground(new Color(10,70,150));
        p4.setBackground(new Color(10,70,100));

        p1.add(l);
        p2.add(nw);
        p2.add(textField);
        p3.add(co);
        p4.add(e);

        this.add(p1);
        this.add(p2);
        this.add(p3);
        this.add(p4);
    }
}
