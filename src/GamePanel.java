//Nev:Orosz Arnold-Daniel
//Csoport:513
//Azonosito:oaim1856

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class GamePanel extends JPanel {

    private Model model;
    BufferedImage img[];

    public GamePanel(){
        model=new Model();
        this.img=new BufferedImage[12];

        for(int i=0;i<12;i++) {
            try {
                img[i] = ImageIO.read(new File(i+".png"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.setLayout(null);
    }

    public void setSize(int size){
        model.setSize(size);
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        if(model.isJatek()) {
            model.getL().setText("Remaining marks:" + model.getIbszam());
        }else{
            if(nyert()){
                model.getL().setText("Game won!");
                model.getTimer().cancel();
            }else {
                model.getL().setText("Game lost!");
                model.getTimer().cancel();
            }
        }
        for (int i = 0; i < model.getSize(); i++) {
            for (int j = 0; j < model.getSize(); j++) {
                if(model.getClicked()[i][j]==0) {
                    g.drawImage(img[10], i * model.getImageSize(), j * model.getImageSize(), this);
                }
                if(model.getClicked()[i][j]==1){
                    g.drawImage(img[model.getSecond()[i][j]], i * model.getImageSize(), j * model.getImageSize(), this);
                }
                if(model.getClicked()[i][j]==2){
                    g.drawImage(img[model.getThird()[i][j]], i * model.getImageSize(), j * model.getImageSize(), this);
                }
            }
        }
    }

    private boolean nyert(){
        for(int i=0;i<model.getSize();i++){
            for(int j=0;j<model.getSize();j++){
                if(model.getSecond()[i][j]!=9&&model.getClicked()[i][j]!=1){
                    return false;
                }
            }
        }
        return true;
    }

    public Model getModel() {
        return model;
    }

}
