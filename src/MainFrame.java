//Nev:Orosz Arnold-Daniel
//Csoport:513
//Azonosito:oaim1856

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    public MainFrame(){
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(100,10,765,818);
        this.setResizable(false);
        this.setBackground(new Color(10,150,150));

        JPanel jp=new JPanel();
        this.add(jp);

        CardLayout c=new CardLayout();
        jp.setLayout(c);

        Panel p=new Panel(c,jp);
        Menu m=new Menu(c,jp,p);
        jp.add(p,"2");
        jp.add(m,"1");

        c.show(jp,"1");

        this.setVisible(true);
    }

    public static void main(String [] args){
        MainFrame m=new MainFrame();
    }
}
