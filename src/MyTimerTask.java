//Nev:Orosz Arnold-Daniel
//Csoport:513
//Azonosito:oaim1856

import java.awt.*;
import java.util.TimerTask;

public class MyTimerTask extends TimerTask {

    private Panel panel;
    private Label label;

    public MyTimerTask(Label label,Panel panel){
        this.label=label;
        this.panel=panel;
    }

    @Override
    public void run() {
        panel.setsp(panel.getsp()+1);
        label.setText("Elapsed time: "+panel.getsp());
    }

}
