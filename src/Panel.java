//Nev:Orosz Arnold-Daniel
//Csoport:513
//Azonosito:oaim1856

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.Random;
import java.util.Timer;

public class Panel extends JPanel {
    
    private GamePanel gamePanel;

    private int vbszam;
    private int size;
    private final String filenev="allapot.txt";
    private Random r;

    private JPanel jp=new JPanel();
    private Label label=new Label();
    
    private int secondsPassed;

    private Button menu=new Button("Menu");

    public Panel(CardLayout c,JPanel x) {
        jp.setBackground(Color.CYAN);
        jp.setLayout(new GridLayout(2,2));

        gamePanel=new GamePanel();
        gamePanel.getModel().getL().setText("Remaining marks:"+vbszam);

        r=new Random();

        jp.add(gamePanel.getModel().getL());
        jp.add(label);
        jp.add(menu);

        menu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    gamePanel.getModel().getTimer().cancel();
                    FileOutputStream f=new FileOutputStream(filenev);
                    ObjectOutput o=new ObjectOutputStream(f);
                    o.writeObject(size);
                    o.writeObject(gamePanel.getModel().getClicked());
                    o.writeObject(gamePanel.getModel().getSecond());
                    o.writeObject(gamePanel.getModel().getThird());
                    o.writeObject(gamePanel.getModel().isJatek());
                    o.writeObject(gamePanel.getModel().getIbszam());
                    o.writeObject(secondsPassed);
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                c.show(x,"1");
            }
        });

        this.add(gamePanel);
        this.add(jp);
        this.setLayout(null);
        this.addMouseListener(new MinesAdapter());
    }

    public void setk(int clicked[][]){
        for(int i=0;i<size;i++){
            for(int j=0;j<size;j++){
                this.gamePanel.getModel().setClicked(clicked);
            }
        }
    }

    public void setm(int second[][]){
        for(int i=0;i<size;i++){
            for(int j=0;j<size;j++){
                this.gamePanel.getModel().setSecond(second);
            }
        }
    }

    public void seth(int third[][]){
        for(int i=0;i<size;i++){
            for(int j=0;j<size;j++){
                this.gamePanel.getModel().setThird(third);
            }
        }
    }

    public void setj(boolean jatek){
        this.gamePanel.getModel().setJatek(jatek);
    }

    public void seti(int ibszam){
        this.gamePanel.getModel().setIbszam(ibszam);
    }

    public void setsp(int seconsPassed){
        this.secondsPassed=seconsPassed;
        label.setText("Elapsed seconds: "+seconsPassed);
    }

    public int getsp(){
        return secondsPassed;
    }
    
    public void setSize(int size){
        this.size=size;
        vbszam=(size*size)/7;
        jp.setBounds(0,size*gamePanel.getModel().getImageSize(),size*gamePanel.getModel().getImageSize(),30);
        gamePanel.setSize(size);
    }

    public void startTimer(){
        if(gamePanel.getModel().isJatek()) {
            gamePanel.getModel().setTimer(new Timer());
            gamePanel.getModel().getTimer().schedule(new MyTimerTask(label, this), 0, 1000);
        }
    }

    void newgame(){
        secondsPassed=-1;
        gamePanel.getModel().setJatek(true);
        startTimer();
        gamePanel.getModel().setIbszam(vbszam);
        gamePanel.getModel().getL().setText("Remaining marks:" + gamePanel.getModel().getIbszam());

        for(int i=0;i<size;i++){
            for(int j=0;j<size;j++){
                gamePanel.getModel().getClicked()[i][j]=0;
                gamePanel.getModel().getSecond()[i][j]=0;
                gamePanel.getModel().getThird()[i][j]=0;
            }
        }
        int bszam=0;
        while(bszam<vbszam) {
            int i=r.nextInt(size);
            int j=r.nextInt(size);
            if(gamePanel.getModel().getSecond()[i][j]!=9) {
                gamePanel.getModel().getSecond()[i][j] = 9;
                bszam++;
            }
        }
        for(int i=0;i<size;i++){
            for(int j=0;j<size;j++) {
                if (gamePanel.getModel().getSecond()[i][j] != 9) {
                    int bszomsz = 0;
                    int startPosX = (i - 1 < 0) ? i : i - 1;
                    int startPosY = (j - 1 < 0) ? j : j - 1;
                    int endPosX = (i + 1 > size - 1) ? i : i + 1;
                    int endPosY = (j + 1 > size - 1) ? j : j + 1;
                    for (int rowNum = startPosX; rowNum <= endPosX; rowNum++) {
                        for (int colNum = startPosY; colNum <= endPosY; colNum++) {
                            if (gamePanel.getModel().getSecond()[rowNum][colNum] == 9) {
                                bszomsz++;
                            }
                        }
                    }
                    gamePanel.getModel().getSecond()[i][j] = bszomsz;
                }
            }
        }
    }

    @Override
    protected void paintComponent(Graphics g){
        gamePanel.paintComponent(g);
    }

    private boolean nyert(){
        for(int i=0;i<size;i++){
            for(int j=0;j<size;j++){
                if(gamePanel.getModel().getSecond()[i][j]!=9&&gamePanel.getModel().getClicked()[i][j]!=1){
                    return false;
                }
            }
        }
        return true;
    }

    private void felfed(int i , int j){
        if(gamePanel.getModel().getSecond()[i][j]>=0&&gamePanel.getModel().getSecond()[i][j]<=8) {
            if(gamePanel.getModel().getSecond()[i][j]>=1){
                if (gamePanel.getModel().getClicked()[i][j] == 2) {
                    gamePanel.getModel().setIbszam(gamePanel.getModel().getIbszam()+1);
                    gamePanel.getModel().getL().setText("Remaining marks:" + gamePanel.getModel().getIbszam());
                }
                gamePanel.getModel().getClicked()[i][j]=1;
                return;
            }
            if (gamePanel.getModel().getClicked()[i][j] == 2) {
                gamePanel.getModel().setIbszam(gamePanel.getModel().getIbszam()+1);
                gamePanel.getModel().getL().setText("Remaining marks:" + gamePanel.getModel().getIbszam());
            }
            gamePanel.getModel().getClicked()[i][j]=1;
            int startPosX = (i - 1 < 0) ? i : i - 1;
            int startPosY = (j - 1 < 0) ? j : j - 1;
            int endPosX = (i + 1 > size - 1) ? i : i + 1;
            int endPosY = (j + 1 > size - 1) ? j : j + 1;
            for (int rowNum = startPosX; rowNum <= endPosX; rowNum++) {
                for (int colNum = startPosY; colNum <= endPosY; colNum++) {
                    if(!(rowNum==i&&colNum==j)) {
                        if(gamePanel.getModel().getClicked()[rowNum][colNum]!=1) {
                            felfed(rowNum, colNum);
                        }
                    }
                }
            }
        }
    }

    class MinesAdapter extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            if(gamePanel.getModel().isJatek()) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    int x = e.getX();
                    int y = e.getY();
                    
                    int x2 = x / gamePanel.getModel().getImageSize();
                    int y2 = y / gamePanel.getModel().getImageSize();
                    
                    if (gamePanel.getModel().getClicked()[x2][y2] == 2) {
                        gamePanel.getModel().setIbszam(gamePanel.getModel().getIbszam()+1);
                        gamePanel.getModel().getL().setText("Remaining marks:" + gamePanel.getModel().getIbszam());
                    }
                    
                    if (gamePanel.getModel().getSecond()[x2][y2] == 9) {
                        for(int i=0;i<size;i++){
                            for(int j=0;j<size;j++){
                                if(gamePanel.getModel().getSecond()[i][j]==9){
                                    gamePanel.getModel().getClicked()[i][j]=1;
                                }
                            }
                        }
                        gamePanel.getModel().setJatek(false);
                    }
                    
                    gamePanel.getModel().getClicked()[x2][y2] = 1;
                    
                    if(nyert()){
                        gamePanel.getModel().setJatek(false);
                    }

                    if(gamePanel.getModel().getSecond()[x2][y2]==0){
                        felfed(x2,y2);
                    }

                    repaint();
                }

                if (SwingUtilities.isRightMouseButton(e)) {
                    int x = e.getX();
                    int y = e.getY();

                    int x2 = x / gamePanel.getModel().getImageSize();
                    int y2 = y / gamePanel.getModel().getImageSize();
                    
                    if (gamePanel.getModel().getClicked()[x2][y2] != 1) {
                        gamePanel.getModel().getClicked()[x2][y2] = 2;
                        if (gamePanel.getModel().getThird()[x2][y2] != 11) {
                            gamePanel.getModel().getThird()[x2][y2] = 11;
                            gamePanel.getModel().setIbszam(gamePanel.getModel().getIbszam()-1);
                            gamePanel.getModel().getL().setText("Remaining marks:" + gamePanel.getModel().getIbszam());
                        } else {
                            gamePanel.getModel().getThird()[x2][y2] = 0;
                            gamePanel.getModel().setIbszam(gamePanel.getModel().getIbszam()+1);
                            gamePanel.getModel().getClicked()[x2][y2] = 0;
                            gamePanel.getModel().getL().setText("Remaining marks:" + gamePanel.getModel().getIbszam());
                        }
                        
                        repaint();
                    }
                }
            }
        }
    }
}