//Nev:Orosz Arnold-Daniel
//Csoport:513
//Azonosito:oaim1856

import java.awt.*;
import java.util.Timer;

public class Model {

    private boolean jatek;
    private Label l;
    private int size;
    private int[][] clicked;
    private int imageSize=15;
    private int[][] second;
    private int[][] third;
    private int ibszam;
    private Timer timer;

    public Model(){
        l=new Label();
        timer=new Timer();
    }

    public void setJatek(boolean jatek) {
        this.jatek = jatek;
    }

    public void setL(Label l) {
        this.l = l;
    }

    public void setSize(int size) {
        this.size = size;
        this.clicked=new int[size][size];
        this.third=new int[size][size];
        this.second=new int[size][size];
    }

    public void setClicked(int[][] clicked) {
        for(int i=0;i<size;i++){
            for(int j=0;j<size;j++){
                this.clicked[i][j]=clicked[i][j];
            }
        }
    }

    public void setImageSize(int imageSize) {
        this.imageSize = imageSize;
    }

    public void setSecond(int[][] second) {
        for(int i=0;i<size;i++){
            for(int j=0;j<size;j++){
                this.second[i][j]=second[i][j];
            }
        }
    }

    public void setThird(int[][] third) {
        for(int i=0;i<size;i++){
            for(int j=0;j<size;j++){
                this.third[i][j]=third[i][j];
            }
        }
    }

    public void setIbszam(int ibszam) {
        this.ibszam = ibszam;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public boolean isJatek() {
        return jatek;
    }

    public Label getL() {
        return l;
    }

    public int getSize() {
        return size;
    }

    public int[][] getClicked() {
        return clicked;
    }

    public int getImageSize() {
        return imageSize;
    }

    public int[][] getSecond() {
        return second;
    }

    public int[][] getThird() {
        return third;
    }

    public int getIbszam() {
        return ibszam;
    }

    public Timer getTimer() {
        return timer;
    }
}
